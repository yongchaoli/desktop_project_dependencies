To use this project, change the version to 1.2 in your pom.xml.

Changes: 

L.19 \<version\>1.2\</version\>

Updated the version of each dependency.

L. 34 \<maven.compiler.source\>13\</maven.compiler.source\>

L. 35 \<maven.compiler.target\>13\</maven.compiler.target\>

L.105 \<version\>13\</version\>

L.110 \<version\>13\</version\>

L.115 \<version\>13\</version\>

L.120 \<version\>13\</version\>


Added: 

L.166 \<shadedArtifactAttached\>true\</shadedArtifactAttached\>

L.166 \<!-- Any name that makes sense --\>

L.166 \<shadedClassifierName\>jackofall\</shadedClassifierName\> 
